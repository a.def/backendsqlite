# Trailblazer - Find a job that fits your desires with OpenAI - Backend

Simple backend architecture of a mobile app project, with SQL database. Introduction to backend dev and database creation and management, so it could be improved a lot, especially when it comes to security

The backend can register new users, log in users thanks to password (passwords are hashed in DB) and keeps them logged in with JSON web tokens, can be set up to querry OpenAI to make job career paths based on user given inputs (by default, off) 

.env contains important information that the user should not have acess to.
<!-- https://chamilo.grenoble-inp.fr/courses/PHELMA4PMIAWM1/document/tp/tp6/tp6.html -->


Here are some examples of commands to try exploring the most basic aspects of the projet. 

## Travailler sur machine locale en mode dev
### Lancement de la machine locale
```
$ cd <racine du projet>/backendsqlite/
$ npm install
$ npm run updatedb
$ npm run startdev

```
### Afficher le site et tester la MVC
Dans un nouveau terminal :
```
$ firefox -new-window http://localhost:3000/bmt/users
```
Puis lancer l'extension [Resting](https://addons.mozilla.org/fr/firefox/addon/resting/) pour réaliser les tests

**1) Création d'un nouvel utilisateur**

	
* 	Méthode : POST
*	Adresse : http://localhost:3000/bmt/users/
*	Body (type x-www-form-urlencoded) : 
data =  {"username":"identifiant", "userEmail":"identifiant.de.test@appliweb.net", "userBirth":"2022", "userPswrd":"azertyA!1@zz"} 

**2) Login**

* Méthode : POST
*	Adresse : http://localhost:3000/bmt/users/login
*	Body (type x-www-form-urlencoded) : 
data =  {"username":"identifiant", "userPswrd":"azertyA!1@zz"}
* Response : récuperer le token renvoyé 

> Ce token est à placer dans les Headers, sous le nom x-access-token. <font color="red">Attention : il faut retirer les guillemets qui encadre le token renvoyé par le serveur </font>

**3) Faire une requête openAI**

* Méthode : POST
*	Adresse : http://localhost:3000/bmt/users/:identifiant/jobcards
* Headers : x-access-token
*	Body (type x-www-form-urlencoded) : 
data =  {"criteria1":"technologie", "criteria2":"agriculture", "criteria3":"campagne"}
* Response : renvoie 2 jobs correspondants aux critères 

**4) Obtenir les job cards d'un utilisateur**

>Il faut être loggé pour avoir accès à ses jobs cards. C'est à dire, indiquer dans les headers son x-access-token ainsi que dans l'adresse URL son identifiant (obtenable avec GET sur ''/bmt/whoami' sans aucun argument dans le body, mais avec le token en entête, bien-sûr).

* Méthode : GET
*	Adresse : http://localhost:3000/bmt/users/:identifiant/jobcards
* Headers : x-access-token
* Response : renvoie un tableau de d'objets anonymes avec la structure des jobCards de la base de données

**5) Obtenir une job card en particulier**

* Méthode : GET
*	Adresse : http://localhost:3000/bmt/users/:identifiant/jobcards/:jobId
* Headers : x-access-token
* Response : un objet anonyme correspondant à l'Id avec la structure des jobCards de la base de données

**5) Supprimer une ou toutes les jobCards**

* Méthode : DELETE
*	Adresse : http://localhost:3000/bmt/users/:identifiant/jobcards/:jobId
* Headers : x-access-token

> Pour supprimer toutes les job cards, se placer à l'adresse '/bmt/users/:identifiant/jobcards'
