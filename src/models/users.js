const Sequelize = require('sequelize');
const jobCards = require('./jobCards');
const db = require('./database.js');
const users = db.define('users', {
  userId: {
    primaryKey: true,
    type: Sequelize.INTEGER,
    autoIncrement: true,
  },
  username: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
    validate: {
      is: /^[A-Za-z\-]{2,16}$/, //TODO: AJOUTER DES NOMBRES A L INDENTIFIANT!!
    }
  },
  userEmail: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
    validate: {
      isEmail: true,
    }
  },
  userBirth: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  userPswrd: {
    type: Sequelize.STRING,
    allowNull: false,
  }
});
// jobCards.belongsTo(users, {
//   // un tag est affilié à un métier précis
//   foreignKey: 'userId',
//   onDelete: 'SET NULL', // supprime les criteria si les jobs auxquels ils étaient rattachés sont supprimés
//   onUpdate: 'CASCADE',
// }); //TODO : fix les relations entre les tables
users.hasMany(jobCards, {
  // un tag est affilié à un métier précis
  foreignKey: 'userId',
  onDelete: 'SET NULL', // supprime les criteria si les jobs auxquels ils étaient rattachés sont supprimés
  onUpdate: 'CASCADE',
});

module.exports = users;
