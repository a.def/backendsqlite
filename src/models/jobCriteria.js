const Sequelize = require('sequelize');
const db = require('./database.js');
const jobCards = require('./jobCards.js');


//Table sauvegardant les critères de recherche utilisés pour trouver un job
const jobCriteria = db.define('jobCriteria', {
  id: {
    primaryKey: true,
    type: Sequelize.INTEGER,
    autoIncrement: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      len: [2,64],
    },
  },
  coordinates: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  description: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  /*
  jobId: { // clé étangère désignant le job auquel sera rattaché le tag
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
  },
  */
});



module.exports = jobCriteria;
