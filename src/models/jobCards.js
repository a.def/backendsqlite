const Sequelize = require('sequelize');
const db = require('./database.js')
const jobCriteria = require('./jobCriteria.js')
const jobRequirements = require('./jobRequirements.js')
const jobCards = db.define('jobCards', {
  jobId: {
    primaryKey: true,
    type: Sequelize.INTEGER,
    unique: true,
    autoIncrement: true,
  },
  jobName: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      len: [1,64],
    },
  },
  jobCoordinates: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  jobDescription: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  jobSalary: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  jobRequirement: {
    type: Sequelize.STRING,
    allowNull: true,
  },

});

jobCards.hasMany(jobCriteria, {
    // un critère est affilié à un métier précis
    foreignKey: 'jobId',
    onDelete: 'SET NULL', // supprime les jobCriteria si les jobs auxquels ils étaient rattachés sont supprimés
    onUpdate: 'CASCADE',
  });
jobCriteria.belongsTo(jobCards); //TODO : fix les relations entre les tables

jobCards.hasMany(jobRequirements, {
  // un requirement est affilié à un métier précis
  foreignKey: 'jobId',
  onDelete: 'SET NULL', // supprime les jobRequirements si les jobs auxquels ils étaient rattachés sont supprimés
  onUpdate: 'CASCADE',
})
jobRequirements.belongsTo(jobCards); //TODO : fix les relations entre les tables


module.exports = jobCards;
