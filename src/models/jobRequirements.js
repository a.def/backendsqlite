const Sequelize = require('sequelize');
const db = require('./database.js');
const jobCards = require('./jobCards.js');

//Table conservant l'expérience et le parcours scolaire et universitaire nécessaire pour accéder au job
const jobRequirements = db.define('jobRequirements', {
  id: {
    primaryKey: true,
    type: Sequelize.INTEGER,
    autoIncrement: true,
  },
  coordinates: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  description: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  /*
  jobId: { // clé étangère désignant le job auquel sera rattaché le tag
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
  },
  */
});



module.exports = jobRequirements;
