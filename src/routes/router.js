const router = require('express').Router()
router.use(require('./openAIEndpoint'));
router.use(require('./user'));
router.use(require('./jobCards'));
router.use(require('./jobCriteria'));

module.exports = router;
