const express = require('express');
const router = express.Router();
const jobCriteria = require('../controllers/jobCriteria.js');

const userVerifications = require('../security/users.js');
const users = require('../controllers/users.js');
const openAIHandler = require('../controllers/OpenAIHandler.js');

const methodNotAllowed = (req, res, next) => res.status(405).send();

router
//Définition de la route....
.route('/bmt/jobCriteria/') 
//Liste des methodes acceptés 
.get(jobCriteria.getJobCriteria)
//Pour toute autre route, on renvoie une erreur interdiction d'accès.
.all(methodNotAllowed);

router
.route('/bmt/jobCriteria/:criteriaId') 
//Liste des methodes acceptés 
.get(jobCriteria.getJobCriteria)
//Pour toute autre route, on renvoie une erreur interdiction d'accès.
.all(methodNotAllowed);


router
.route('/bmt/users/:username/jobCards/:jobId/criteria/') 
.get( //Get the card criteria
    userVerifications.verifyToken,
    userVerifications.verifyUsername,
    userVerifications.verifyURLUsername,
    jobCriteria.getJobCriteria
)
.all(methodNotAllowed);

// router
// .route('/bmt/users/:username/jobCriteria/') 
// .get( //Get all criteria used by the user
//     userVerifications.verifyToken,
//     userVerifications.verifyUsername,
//     userVerifications.verifyURLUsername,
//     jobCriteria.getjobCriteria
// )
// .all(methodNotAllowed);



module.exports = router