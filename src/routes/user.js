const express = require('express');
const router = express.Router();
const users = require('../controllers/users.js');
const verifications = require('../security/users.js');
const encryption = require('../security/encryption.js');

const methodNotAllowed = (req, res, next) => res.status(405).send();

//Routes pour les utilisateurs


//Pour bmt/users/ : getusers et create new users
router
//Définition de la route....
.route('/bmt/users/') 
//Liste des methodes acceptés 
.post(
    verifications.parseReqBodyData,
    verifications.verifyNewUserInput, //Verifie que l'input a bien les champs voulus
    verifications.isNewUsernameAvailable,  //verifie que l'username est conforme aux regles
    verifications.isNewUserEmailAvailable,
    verifications.isUserPswrdStrongEnough,
    encryption.encryptUserPswrd,
    users.addNewUser)
.delete(
    verifications.whataFoolYouAreIamAGod, //Verification si on est bien un dieu du serveur, capable de supprimer tant de vies en un claquement de doigt...
    users.bringPeaceToTheGalaxy //Suppression
)
.get(users.getUsers) //FIXME: no one should be able to retrieve this information
//Pour toute autre route, on renvoie une erreur interdiction d'accès.
.all(methodNotAllowed);


//Pour bmt/users/login
router
.route('/bmt/users/login')
.post(
    verifications.parseReqBodyData,
    verifications.verifyUsername,
    users.getHash,
    encryption.verifyUserPswrd,
    users.getToken
)
.all(methodNotAllowed); //TODO: un moyen de savoir si on se connecte avec email ou username

//Pour /bmt/whoami
router
.route('/bmt/whoami')
.get(verifications.verifyToken, 
    verifications.verifyUsername, 
    users.whoAmI)
.all(methodNotAllowed);

router
.route('/bmt/users/:username/')
.delete(
    verifications.verifyToken,
    verifications.verifyUsername,
    verifications.verifyURLUsername,
    users.deleteUser
)
.all(methodNotAllowed);

router
.route('/bmt/users/:username/editUsername')
.put(
    verifications.verifyToken,
    verifications.verifyUsername,
    verifications.verifyURLUsername,
    verifications.parseReqBodyData,
    users.updateUsername,
    users.getToken //appalé si on change l'username car il faut alors remplacer le token
)
.all(methodNotAllowed);

router
.route('/bmt/users/:username/editUserEmail')
.put(
    verifications.verifyToken,
    verifications.verifyUsername,
    verifications.verifyURLUsername,
    verifications.parseReqBodyData,
    users.updateUserEmail,
)
.all(methodNotAllowed);

router
.route('/bmt/users/:username/editUserBirth')
.put(
    verifications.verifyToken,
    verifications.verifyUsername,
    verifications.verifyURLUsername,
    verifications.parseReqBodyData,
    users.updateUserBirth,
)
.all(methodNotAllowed);

router
.route('/bmt/users/:username/editUserPswrd')
.put(
    verifications.verifyToken,
    verifications.verifyUsername,
    verifications.verifyURLUsername,
    verifications.parseReqBodyData,
    verifications.verifyEditPswrdInput,
    users.getHash,
    encryption.verifyUserPswrd,
    verifications.isUserPswrdStrongEnough,
    encryption.encryptUserPswrd,
    users.updateUserPswrd)
.all(methodNotAllowed);


//TODO: LES TRUCS A FAIRE

//LOGGING OUT SE FAIT COTE CLIENT : SUPPRESSION SIMPLE DU TOKEN
/*
router
.route('/bmt/users/:username/logout')
.delete(
    verifications.verifyToken, 
    verifications.verifyUsername,
    users.destroyToken
)
*/



module.exports = router
