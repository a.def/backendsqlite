const express = require('express');
const router = express.Router();
const verifications = require('../security/jobCards.js');
const openAIHandler = require('../controllers/OpenAIHandler.js');

router.get('/', verifications.parseReqQueryData, openAIHandler.getOpenAIResponse);

module.exports = router;
