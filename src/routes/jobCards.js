const express = require('express');
const router = express.Router();
const jobCards = require('../controllers/jobCards.js');
const jobVerifications = require('../security/jobCards.js');
const userVerifications = require('../security/users.js');
const users = require('../controllers/users.js');
const openAIHandler = require('../controllers/OpenAIHandler.js');

const methodNotAllowed = (req, res, next) => res.status(405).send();

router
//Définition de la route....
.route('/bmt/jobCards/') 
//Liste des methodes acceptés 
.post(
    jobVerifications.parseReqBodyData,
    jobVerifications.verifyNewJobCardInput, //Verifie que l'input a bien les champs voulus
    jobCards.manualNewJobCard
)
.delete(
    jobVerifications.whataFoolYouAreIamAGod, //Verification si on est bien un dieu du serveur, capable de supprimer tant de vies en un claquement de doigt...
    jobCards.bringPeaceToTheGalaxy //Suppression
)

.get(jobCards.getJobCards)
//Pour toute autre route, on renvoie une erreur interdiction d'accès.
.all(methodNotAllowed);

router
.route('/bmt/users/:username/jobcards/search') //utilisation de queries http : req.query = {criteria = '<jobCriteria.name>'}

.get(//verif relatives a utilisateur
    userVerifications.verifyToken,
    userVerifications.verifyUsername,
    userVerifications.verifyURLUsername,
    jobVerifications.verifyURLSearch,
    jobCards.searchJobCards,
    jobCards.getJobCards,
    )
.all(methodNotAllowed);

router
.route('/bmt/users/:username/jobcards') 
.get(
    userVerifications.verifyToken,
    userVerifications.verifyUsername,
    userVerifications.verifyURLUsername,
    jobCards.getJobCards
)
.post(
    //verif relatives a utilisateur
    userVerifications.verifyToken,
    userVerifications.verifyUsername,
    userVerifications.verifyURLUsername,
    //verif input : a bien les champs criteria1, criteria2 et criteria3
    jobVerifications.parseReqBodyData,
    //openAI
    openAIHandler.verifyInput,
    openAIHandler.getOpenAIResponse,
    //add job to database
    jobCards.addNewJobCard
)
.delete(
    //verif user
    userVerifications.verifyToken,
    userVerifications.verifyUsername,
    userVerifications.verifyURLUsername,
    //task
    jobCards.deleteJobCard
    )
.all(methodNotAllowed);

router
.route('/bmt/users/:username/jobcards/:jobId') 
.delete(
    //verif user
    userVerifications.verifyToken,
    userVerifications.verifyUsername,
    userVerifications.verifyURLUsername,
    //task
    jobCards.deleteJobCard
)
.get(//verif relatives a utilisateur
    userVerifications.verifyToken,
    userVerifications.verifyUsername,
    userVerifications.verifyURLUsername,
    jobCards.getJobCards,
    )
.all(methodNotAllowed);



//TODO : Trucs a ajouter
/*



*/


module.exports = router