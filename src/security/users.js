const status = require('http-status')
const jws = require('jws')
const userModel = require('../models/users.js')
const has = require('has-keys')
const CodeError = require('../util/CodeError.js')
const { assert } = require('../util/assert')
require('mandatoryenv').load(['SECRET'])
const { SECRET } = process.env
const { GIFT } = process.env
/*
const assert = function(conditionToCheck, errorMessage, errorType = status.BAD_REQUEST) {
  if (!conditionToCheck) throw new CodeError(errorMessage || '', errorType);
}
*/


module.exports = {

  async verifyToken(req, res, next){
    //vérification de la présence d'un header et d'un token
    if (!req.headers || !req.headers.hasOwnProperty('x-access-token')) throw {code: 403, message: 'Missing x-access-token'} 
    //On accède au token et on vérifie sa validité
    const token = req.headers["x-access-token"]
    if(!jws.verify(token, 'HS256', SECRET)) throw {code: 403, message: 'Invalid Token'}
    //On passe le username dans la request
    req.data =  JSON.parse(jws.decode(token).payload) // req.data = {username: 'Traveeler', userRole: 'user', userId: 5}
    //On appelle le prochain test
    next()
  },
  
  async verifyUsername(req, res, next){
    //Verifie que username est dans data
      const found = await userModel.findOne({ where: { username : req.data.username } })
      assert(found, 'User not found.') //TODO CYPRESS
      req.data.userId = found.userId;
      req.user = found;
      next()
  },
  

  async verifyURL(req, res, next){
    if (!has(req.params, 'user')) throw new CodeError('You must specify the user', status.BAD_REQUEST);
    if (req.username != req.params.user) throw new CodeError('The username in URL does not match with the token', status.BAD_REQUEST);
    next();
  },

  async parseReqBodyData(req, res, next) {
    try {
      req.data = {...req.data, ...JSON.parse(req.body.data)}; //On merge les données de req.body.data avec celle de req.data, si req.data existe déjà
      //A noter que si req.data et JSON.parse(req.body.data) ont les mêmes attributs, les attributs après le merge auront les valeurs de ceux de JSON.parse(req.body.data)
    }
    catch {
      throw new CodeError('ParsingError while parsing', status.BAD_REQUEST);
    }
    next();
  },

  async verifyNewUserInput(req, res, next){
    assert(has(req.data, 'username'), 'You must specify your username');
    assert(/^[a-zA-Z]*$/.test(req.data.username), "The username contains illegal characters.");
    assert(has(req.data, 'userEmail'), 'You must specify your email');
    assert(/^[a-zA-Z0-9.-]*@[a-zA-Z0-9-.]*[.]+[a-zA-Z]*$/.test(req.data.userEmail), "Non standard email address.");
    assert(has(req.data, 'userPswrd'), 'You must specify a password')
    next();
  },

  async isNewUsernameAvailable(req, res, next){
    const found = await userModel.findOne({where : {username : req.data.username}});
    if (found) {
      throw new CodeError('Username already used.', status.BAD_REQUEST);
    }
    next();
  },
  
  async isNewUserEmailAvailable(req, res, next){
    const found = await userModel.findOne({where : {userEmail : req.data.userEmail}});
    if (found) {
      throw new CodeError('Email already used.', status.BAD_REQUEST);
    }
    next();
  },

  async isUserPswrdStrongEnough(req, res, next){
    assert(typeof(req.data.userPswrd) == 'string', "Wrong password format.");
    assert(req.data.userPswrd.length >= 8, "Password too weak.");
    assert(/[a-z]/.test(req.data.userPswrd), "Password too weak : needs at least 1 lower case caracter.");
    assert(/[A-Z]/.test(req.data.userPswrd), "Password too weak : needs at least 1 upper case caracter.");
    assert(/[0-9]/.test(req.data.userPswrd), "Password too weak : needs at least 1 digit.");
    assert(/[\s|&()-+%/*@$€§'_,.;:!?-]/.test(req.data.userPswrd), "Password too weak : needs at least 1 special character such as |& ()-+%/*$€§,.;:!?-).");
    assert(/^[\sa-zA-Z0-9@|&()-+_%/*$€§',.;:!?-]*$/.test(req.data.userPswrd), "The password contains illegal characters.");
    next();
  },


  async verifyEditPswrdInput(req, res, next) {
    assert(has(req.data, 'newUserPswrd'), 'You must specify the new password');
    assert(has(req.data, 'currentUserPswrd'), 'You must specify the current password')
    req.pswrdEdit = true; //pour que verifyUserPswrd sache qu'il faut modifier un nouveau mot de passe
    req.data.userPswrd = req.data.currentUserPswrd;
    next();
  },

  async verifyURLUsername(req, res, next){
    if (!has(req.params, 'username')) throw new CodeError('You must specify the username', status.BAD_REQUEST)
    if (req.data.username != req.params.username) throw new CodeError('The username in URL does not match with the token', status.BAD_REQUEST)
    next()
  },

  async whataFoolYouAreIamAGod(req, res, next) {
    //Fonction qui vérifie si tu es bien un dieu du serveur, capable semer des graines de vie, les faire fleurir, tout comme de les cueillir avant la fin du printemps !
    //A place where destiny is made
    assert(has(req.body, 'divineGift'), "You don't belong here. Go away." );
    if (req.body.divineGift != GIFT) {
      console.log("Unauthorized access: " + req.ip)
      throw new CodeError("What a fool you are! I am a God! How can you kill a God? What a grand and intoxicating innocence! How can you be so naive? There is no escape. No recall or intervention can work in this place. Come, lay down your weapons - it is not to late for my mercy.", status.FORBIDDEN)
    }
    
    console.log("Welcome, Artistocrat");
    req.godMode = true;
    next();
    },


}
