const status = require('http-status')
const jws = require('jws')
const jobCardsModel = require('../models/users.js')
const has = require('has-keys')
const CodeError = require('../util/CodeError.js')
const { assert } = require('../util/assert')
require('mandatoryenv').load(['SECRET'])
const { GIFT } = process.env

module.exports = {

    async parseReqBodyData(req, res, next) {
        try {
          req.data = {...req.data, ...JSON.parse(req.body.data)}; //On merge les données de req.body.data avec celle de req.data, si req.data existe déjà
          //A noter que si req.data et JSON.parse(req.body.data) ont les mêmes attributs, les attributs après le merge auront les valeurs de ceux de JSON.parse(req.body.data)
        }
        catch {
          throw new CodeError('ParsingError while parsing', status.BAD_REQUEST);
        }
        next();
    },

    async parseReqQueryData(req, res, next) {
        try {
          req.data = {...req.data, ...JSON.parse(req.query.data)}; //On merge les données de req.body.data avec celle de req.data, si req.data existe déjà
          //A noter que si req.data et JSON.parse(req.body.data) ont les mêmes attributs, les attributs après le merge auront les valeurs de ceux de JSON.parse(req.body.data)
        }
        catch {
          throw new CodeError('ParsingError while parsing', status.BAD_REQUEST);
        }
        next();
    },

    async verifyNewJobCardInput( req, res, next) {
        assert(has(req.data, 'jobName'), 'You must specify your job title');
        assert(/^[a-zA-Z]*$/.test(req.data.jobName), "The job title contains illegal characters.");
        assert(has(req.data, 'jobDescription'), 'You must specify the job description');
        if (has(req.data, 'jobCoordinates')) {
            //TODO : verification de la forme des jobCoords!!
            //assert(/^[0-9:.]*$/.test(req.data.jobCoordinates), "The jobCoordinates contain illegal characters.");
        }
        next();
    },

    async whataFoolYouAreIamAGod(req, res, next) {
        //Fonction qui vérifie si tu es bien un dieu du serveur, capable semer des graines de vie, les faire fleurir, tout comme de les cueillir avant la fin du printemps !
        //A place where destiny is made
        assert(has(req.body, 'divineGift'), "You don't belong here. Go away." );
        assert(req.body.divineGift == GIFT, "What a fool you are! I am a God! How can you kill a God? What a grand and intoxicating innocence! How can you be so naive? There is no escape. No recall or intervention can work in this place. Come, lay down your weapons - it is not to late for my mercy.")
        console.log("Welcome, Artistocrat");
        req.godMode = true;
        next();
    },

    async verifyURLSearch(req,res, next) {
      assert(has(req.query, 'criteria'), 'you must specify a criteria')
      req.search = true;
      next();
    }

}
