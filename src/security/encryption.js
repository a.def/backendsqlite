const bcrypt = require('bcrypt');
const saltRounds = 10;


module.exports = {

    async encryptUserPswrd(req, res, next) {
        //genSalt : génère et renvoie un nombre aléatoire salt en fonction du nombre de tour saltRounds
        //Plus saltRounds est grand, plus le hashing sera imprévisible donc securisé, mais plus la fonction
        //prendra du temps : quelques microsecondes pour saltRounds < 10 à quelques secondes voire quelques heures lorsque saltRounds augmente !!
        bcrypt.genSalt(saltRounds, (err, salt) => {
            bcrypt.hash(req.data.userPswrd, salt, (err, hash) => {
                // hash : genere et renvoie hash, le hash de pswrd
                req.data.userPswrd = hash;
                next();
            });
        });
        
    },

    async encryptPswrd(password) {
        //genSalt : génère et renvoie un nombre aléatoire salt en fonction du nombre de tour saltRounds
        //Plus saltRounds est grand, plus le hashing sera imprévisible donc securisé, mais plus la fonction
        //prendra du temps : quelques microsecondes pour saltRounds < 10 à quelques secondes voire quelques heures lorsque saltRounds augmente !!
        bcrypt.genSalt(saltRounds, (err, salt) => {
            bcrypt.hash(password, salt, (err, hash) => {
                // hash : genere et renvoie hash, le hash de pswrd
                return hash;
            });
        });
        
    },

    async verifyUserPswrd(req, res, next) {
        bcrypt.compare(req.data.userPswrd, req.data.hash, function(err, correctPswrd) {
            if (correctPswrd) {
                if (req.hasOwnProperty('pswrdEdit') && req.pswrdEdit){
                    req.data.userPswrd = req.data.newUserPswrd; //si on est en mode edition du mdp, on file le nouveau mdp dans le champ mdp pour le prochain middleware
                }
                next();
            }
            else {
                
                //Login failed
                //TODO: exmple de gestion d'authentification : https://stackoverflow.com/questions/8726473/node-js-issue-with-res-redirect-in-middleware
                //TODO: res.redirect('/login?failed=true'); ????
                res.json({ status: false, message: 'Wrong password' });
                //res.redirect('/login?failed=true');
                }
            }
        )
    }
}

//https://sebhastian.com/bcrypt-node/
//https://medium.com/@manishsundriyal/a-quick-way-for-hashing-passwords-using-bcrypt-with-nodejs-8464f9785b67