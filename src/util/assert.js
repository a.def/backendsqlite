const status = require('http-status')
const CodeError = require('../util/CodeError.js')

const assert = function(conditionToCheck, errorMessage, errorType = status.BAD_REQUEST) {
  if (!conditionToCheck) throw new CodeError(errorMessage || '', errorType);
}

module.exports = {assert};