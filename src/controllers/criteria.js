import criteriaModel from '../models/criteria';

const jobCardsModel = require('../models/job_cards');

export class criteria {
  #name;
  #description;

  // classe "tag" vouée à stocker les informations issues de la vue en un même objet
  constructor(name, description) {
    this.#name = name;
    this.#description = descript;
  }

  get getDescription() {
    return this.#description;
  }

  get getName() {
    return this.#name;
  }

  set setDescription(value) {
    this.#description = value;
  }
}

module.exports = {
  /*function createTagFromAPIResponse(){
    //TODO : récupérer les infos issues de la réponse d'openAI pour créer un tag
    let tag
},*/
  async createTagInDB(JSTagObect, IDFromCreatedJob) {
    if (JSTagObect != undefined) {
      await criteriaModel.create({
        // on crée le tag2 associé à la carte créée ci dessus, s'il est donné
        // TODO : finir l'ajout des criteria (créer une classe tag qui comprend la description ?)
        name: JSTagObect.name,
        description: JSTagObect.description,
        job_id: IDFromCreatedJob,
      });
    }
  },
};

/*async newJobCard(description, jobName, tag1, tag2, tag3) {
    // création d'un nouvel utilisateur
    await jobCardsModel.create({ //TODO : faire en sorte que les criteria puissent être ajoutés à la création de la carte
      description: description,
      job_name: jobName,
    });
    console.log('Nouvelle carte ajoutée');
  },*/
