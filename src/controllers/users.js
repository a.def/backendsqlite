const userModel=require('../models/users');
const CodeError = require('../util/CodeError.js');
const jws = require('jws');
const status = require('http-status')
const has = require('has-keys');
const { assert } = require('../util/assert');
const { SECRET } = process.env;

module.exports = {


  async addNewUser (req, res) {
    // #swagger.tags = ['Users']
    // #swagger.summary = 'Adds a New User'
    // #swagger.parameters['obj'] = { in: 'body', description:'data', schema: { $data: 'John Doe' }}
    await userModel.create(req.data); //data est de la forme {username:<username>} donc peut directement être utilisé par create()
    res.json({ status: true, message: 'User Added' });
  },

  async getUsers (req, res) {
    // #swagger.tags = ['Users']
    // #swagger.summary = 'Get All users'
    const data = await userModel.findAll({ include: { all: true, nested: true }})

    res.json({ status: true, message: 'Returning users', data })
  },

  async getToken(req, res){
    //TODO: Gestion de tokens sécurisés. Explication à l'adresse suivante :
    //https://website.simplx.fr/blog/2016/09/27/authentification-api-via-jwt-et-cookies/
    if (!has(req.data, 'userId')) throw new CodeError('You must specify the userId', status.BAD_REQUEST); 
    const found = await userModel.findOne({ where: { userId : req.data.userId } })
    if (!found) throw new CodeError('This user doesnt exist', status.BAD_REQUEST)
    res.status(200).json({ 
      access_token : jws.sign({
        header: {alg: 'HS256'},
        payload: {
          username : found.username,
          userRole : 'user',
          userId : found.userId,
        },

        secret: SECRET
      })
      //TODO: Si le temps : ajout de refresh token pour rendre les connexions un peu plus securisées. Exemple ici: https://www.izertis.com/en/-/refresh-token-with-jwt-authentication-in-node-js
    })
  },

  async getHash(req, res, next) {
    const user = await userModel.findOne({ where: { userId : req.data.userId } });
    req.data.hash = user.userPswrd;
    next();
  },

  async whoAmI(req, res) {
    //Est lancé après verifyToken
    res.status(200).json({
      data:  req.data.username  //on renvoie une réponse de la forme {"data": "LOGIN_DÉCODÉ"}})
    })
  },

  async updateUsername(req, res, next) {
    if (!has(req.data, 'newUsername')) throw new CodeError('You must specify the new username', status.BAD_REQUEST)
    await userModel.update( { username : req.data.newUsername }, {where : { userId : req.data.userId}});
    next();
  },

  async updateUserEmail(req, res) {
    if (!has(req.data, 'newUserEmail')) throw new CodeError('You must specify the new Email', status.BAD_REQUEST)
    await userModel.update( { userEmail : req.data.newUserEmail }, {where : { userId : req.data.userId }});
    res.status(201).json({ //réponse 201 : Created
      data:  'Email address updated'  //on renvoie une réponse de la forme {"data": "LOGIN_DÉCODÉ"}})
    })
  },

  async updateUserBirth(req, res) {
    if (!has(req.data, 'newUserBirth')) throw new CodeError('You must specify the new birth date', status.BAD_REQUEST)
    await userModel.update( { userBirth : req.data.newUserBirth }, {where : { userId : req.data.userId }});
    res.status(201).json({ //réponse 201 : Created
      data:  'Birth date updated'  //on renvoie une réponse de la forme {"data": "LOGIN_DÉCODÉ"}})
    })
  },

  async updateUserPswrd(req, res) {
    await userModel.update( { userPswrd : req.data.userPswrd }, {where : { userId : req.data.userId }});
    res.status(201).json({ //réponse 201 : Created
      data:  'pswrdate updated'  //on renvoie une réponse de la forme {"data": "LOGIN_DÉCODÉ"}})
    })
  },

  async bringPeaceToTheGalaxy(req, res) {
    // #swagger.tags = ['Users']
    // #swagger.summary = 'Delete User'
    assert(req.godMode == true, "Permission denied");
    await userModel.destroy({ where: { } })
    res.json({ 
      status: true, 
      message: 'Users deleted. Now consider the cost of your actions.',
      data: `
      And on the pedestal these words appear:
      'My name is Ozymandias, king of kings:
      Look on my works, Ye Mighty, and despair!'
      
      Nothing beside remains. Round the decay
      Of that colossal wreck, boundless and bare,
      The lone and level sands stretch far away.
      `
    })
  },

  async deleteUser(req, res){
    await userModel.destroy({ where: { userId : req.data.userId} })
    res.json({ 
      status: true, 
      message: 'Your personal data has successfully been deleted.',
    })
  },
}
