const userModel=require('../models/users');
const jobCardsModel = require('../models/jobCards');
const jobCriteriaModel = require('../models/jobCriteria');
const jobRequirementsModel = require('../models/jobRequirements');
const has = require('has-keys')
const jws = require('jws');
const { assert } = require('../util/assert');
const { hash } = require('bcrypt');

module.exports = {

  async getJobCards (req, res) {
    // #swagger.criteria = ['Users']
    // #swagger.summary = 'Get All users'

    let jobs = {};
    //Cas où on a utilisé un critère de recherche dans l'URL. jobs dàja trouvé dans le middleware précédent.
    if (req.search) jobs = req.data.jobs;
    //Tests confitionnels des cas standards, sans critères de recherche
    else {
      if (has(req.params, 'jobId')) {
        jobs = await jobCardsModel.findOne({ where: { userId : req.data.userId, jobId : parseInt(req.params.jobId)}}, { include: { all: true }})
      }
      else if (has(req.params, 'username')){
        jobs = await jobCardsModel.findAll({ where: { userId : req.data.userId} }, { include: { all: true }})
      }
      else {
        jobs = await jobCardsModel.findAll({ include: { all: true }})
      }
    } 
    //Envoie de la réponse
    if (jobs === null || Object.keys(jobs).length === 0) res.json({ status: 404, message: 'No job card found' })
    else res.json({ status: true, message: 'Returning job cards', jobs })
  },

  async addNewJobCard(req, res) {
    // #swagger.criteria = ['Users']
    // #swagger.summary = 'Adds a New User'
    // #swagger.parameters['obj'] = { in: 'body', description:'data', schema: { $data: 'John Doe' }}
    for (const job of req.data.jobs) {
      //console.log(job)
      await jobCardsModel.create({
        jobName : job.jobName,
        jobDescription : job.jobDescription,
        jobSalary : job.jobSalary,
        jobCriteria : job.jobCriteria,
        jobRequirements : job.jobRequirements,
        userId : req.data.userId,
      }, {include: [jobCriteriaModel, jobRequirementsModel]});
    }
    res.json({ status: true, message: `Job(s) added to the database`, data : req.data.jobs });
  },

  async manualNewJobCard(req, res) {
    // #swagger.criteria = ['Users']
    // #swagger.summary = 'Adds a New User'
    // #swagger.parameters['obj'] = { in: 'body', description:'data', schema: { $data: 'John Doe' }}
    await jobCardsModel.create(req.data); //data est de la forme {username:<username>} donc peut directement être utilisé par create()
    res.json({ status: true, message: 'User Added' });
  },
  

  async bringPeaceToTheGalaxy(req, res) {
    // #swagger.criteria = ['job Cards']
    // #swagger.summary = 'Delete all job cards'
    assert(req.godMode == true, "Permission denied");
    await jobCardsModel.destroy({ where: { } })
    res.json({ 
      status: true, 
      message: 'Jobs deleted. Now consider the cost of your actions.',
      data: `
      And on the pedestal these words appear:
      'My name is Ozymandias, king of kings:
      Look on my works, Ye Mighty, and despair!'
      
      Nothing beside remains. Round the decay
      Of that colossal wreck, boundless and bare,
      The lone and level sands stretch far away.
      `
    })
  },

  async deleteJobCard(req, res) {
    let data;
    //récupère les données à supprimer en fonction des parametres de l URL
    if (has(req.params, 'jobId')) {
      data = await jobCardsModel.findOne({ where: { userId : req.data.userId, jobId : parseInt(req.params.jobId) } })
      await jobCardsModel.destroy({ where: { userId : req.data.userId, jobId : parseInt(req.params.jobId) } });
    }
    else if (has(req.params, 'username')) {
      data = await jobCardsModel.findAll({ where: { userId : req.data.userId } })
      await jobCardsModel.destroy({ where: { userId : req.data.userId } });
    }
    else {
      //S'il n'y a aucun parametre - et bien qu'on ne puisse pas accéder à cette fonction autrement - vu qu'on n'est jamais trop prudent : 
      res.json({ status: 403, message: 'You cannot access this fonction' })
      return false;
    }

    if (data !== null && Object.keys(data).length !== 0) {
      res.json({ status: 200, message: 'job data deleted' })
    }
    else {
      res.json({ status: 404, message: 'No job card found' })
    }
  },
  
  async searchJobCards(req, res, next) {
    const jobs = await jobCardsModel.findAll(
      { include: 
        { 
          model : jobCriteriaModel, 
          required: true, 
          where : { name : req.query.criteria},
        },
        where : {userId : req.data.userId},
      }
    )

    req.data.jobs = jobs;
    next();
  }
};
