const {Configuration, OpenAIApi} = require('openai');
const { assert } = require('../util/assert')
const has = require('has-keys')
const { OPENAI_API_KEY } = process.env
const configuration = new Configuration({
  apiKey: OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);
/*
const instructions = 'Instructions : ' + 'What is 9+10?';
('Give 2 relevant jobs combining the given interests and the ways to get to that job. Also give a short description for each given job as such :  first given criteria -> why the job matches criteria 1  second given criteria -> why the job matches criteria 2 Options pour y parvenir : description du jobRequirements pour arriver au métier');
const example =
  "Example :   Prompt:- finance - technologie  Response :   - Analyste financier -> Spécialiste du placement en Bourse, l'analyste financier a la cote. Son rôle : aider les investisseurs à choisir les valeurs les moins risquées et les plus rentables grâce à des études approfondies.   Finance -> A l'aide de ses compétences en finance, l'analyste financier surveille la santé et la croissance des sociétés cotées en Bourse afin de conseiller les traders, les gérants de portefeuille et les concepteurs de produits financiers  Technologie -> A l'aide de ses connaissances avancées en IA, l'analyste financier propose des prédictions sur la rentabilité des investissements.  jobSalary débutant : environ 2500 €  Options pour y parvenir :   - master d’école de commerce spécialité finance  - master finance à l’université ou dans une école spécialisée  Image : https://www.istockphoto.com/fr/photos/analyste-financier   - Développeur fintech -> La fintech, c'est l'alliance entre le monde de la finance et celui de la technologie.   Finance -> En travaillant pour une plateforme de crowdfunding, le développeur fintech contribue à réunir des investisseurs et des entrepreneurs en apportant sa contribution en matière de développement web.  Technologie -> Le développeur fintech crée des outils permettant aux utilisateurs de gérer leurs finances de manière plus efficace, par exemple en les aidant à comparer les offres de crédit ou à établir un budget. jobSalary débutant : environ 3000 €  Options pour y parvenir :  - DUT GEA option gestion comptable et finanicière - licence finance - master comptabilité";
*/

function generatePrompt(criteria1, criteria2, criteria3) {
  return `Instructions: 
   Donnez 2 emplois pertinents combinant les intérêts donnés et les moyens d'accéder à cet emploi. Fournissez le résultat sous cette forme :
      {
       "jobs":
       [
        {
          "jobName": "Nom du premier métier trouvé",
          "jobDescription": "description concise du métier",
          "jobCriteria":[ {
                        "name": "nom du premier critère de recherche",
                        "description": "description de l'adéquation entre le métier proposé et le premier critère de recherche"
                   },
                   {
                        "name": "nom du deuxième critère de recherche",
                        "description": "description de l'adéquation entre le métier proposé et le deuxième critère de recherche"

                   }],
          "jobSalary": "salaire moyen du métier trouvé",
          "jobRequirements": [{
                          "description": "premier parcours académique pour arriver au métier"
                       },
                       {
                          "description": "deuxième parcours académique pour arriver au métier"
                       },
                       {
                          "description": "troisième parcours académique pour arriver au métier"
                       }]
        }
  
  Example :   
    Prompt:
      - finance
      - technologie
    Response :
      {
       "jobs":
       [
        {
          "jobName": "Développeur fintech",
          "jobDescription": "La fintech, c'est l'alliance entre le monde de la finance et celui de la technologie.",
          "jobCriteria":[ {
                        "name": "Finance",
                        "description": "En travaillant pour une plateforme de crowdfunding, le développeur fintech contribue à réunir des investisseurs et des entrepreneurs en apportant sa contribution en matière de développement web."
                   },
                   {
                        "name": "Technologie",
                        "description": "Le développeur fintech crée des outils permettant aux utilisateurs de gérer leurs finances de manière plus efficace, par exemple en les aidant à comparer les offres de crédit ou à établir un budget."
                   }],
          "jobSalary": "environ 3000 €",
          "jobRequirements": [{
                          "description": "DUT GEA option gestion comptable et finanicière"
                       },
                       {
                          "description": "licence finance"
                       },
                       {
                          "description": "master comptabilité"
                       }]
        },
        {
          "jobName": "Analyste financier",
          "jobDescription": "Spécialiste du placement en Bourse, l'analyste financier a la cote. Son rôle : aider les investisseurs à choisir les valeurs les moins risquées et les plus rentables grâce à des études approfondies.",
          "jobCriteria":[ 
                   {
                        "name": "Finance",
                        "description": "A l'aide de ses compétences en finance, l'analyste financier surveille la santé et la croissance des sociétés cotées en Bourse afin de conseiller les traders, les gérants de portefeuille et les concepteurs de produits financiers."
                   },
                   {
                        "name": "Technologie",
                        "description": "A l'aide de ses connaissances avancées en IA, l'analyste financier propose des prédictions sur la rentabilité des investissements."
                   }
                 ],
          "jobSalary": "environ 2500 €",
          "jobRequirements": [
                       {
                          "description": "master d’école de commerce spécialité finance"
                       },
                       {
                          "description": "master finance à l’université ou dans une école spécialisée"
                       },
                       {
                          "description": "master comptabilité"
                       }
                      ]
        }
       }
        
  Prompt :
    - ${criteria1}
    - ${criteria2}
    - ${criteria3}
  Response :`;
}
module.exports = {

  async verifyInput(req, res, next) {
    assert((has(req.data, 'criteria1') && has(req.data, 'criteria2')), 'You must specify at least 2 search criteria');
    next();
  },

  async getOpenAIResponse(req, res, next) {
    //For offline debug tests
    if (true) {
      const test = `{ 
        "jobs": [
          
          {"jobName": "Développeur web",
          "jobDescription": "les langages web.",
          "jobCriteria": [        
            {"name": "work in a big city", "description": "New York."},        
            {"name": "vin", "description": "production."},
            {"name": "technology", "description": "web."}],
          "jobSalary": "environ 3500 €",
          "jobRequirements": [
            {"description": "DUT informatique"},       
            {"description": "licence informatique"},
            {"description": "master informatique"}]
          },

          {"jobName":"Développeur application mobile",
          "jobDescription":"tablettes.",
          "jobCriteria": [
            {"name": "work in a big city", "description": "New York."},
            {"name":"alcool","description": "suivre les données"},
            {"name":"technology","description": "pécialiste mobile"}
          ],
          "jobSalary":"environ 3000 €",
          "jobRequirements": [
            {"description":"DUT informatique"},
            {"description":"licence informatique"},
            {"description": "master informatique"}
          ]}
        ]}`
      req.data = {...req.data, ...JSON.parse(test)};
      next();
    }

    //for real queries
    else {
    // console.log(JSON.parse(req.query.data).criteria1)
    const response = await openai
      .createCompletion('text-davinci-002', {
        prompt: generatePrompt(
          req.data.criteria1,
          req.data.criteria2,
          req.data.criteria3, //JSON.parse(req.query.data).criteria3,
        ),
        temperature: 1,
        max_tokens: 2000,
        top_p: 1,
        frequency_penalty: 0,
        presence_penalty: 0,
      })
      .then(result => {
        //res.json({status: true, message: result.data.choices[0]});
        //console.log(JSON.parse(result.data.choices[0].text)['métiers'])
        req.data = {...req.data, ...JSON.parse(result.data.choices[0].text)};
        next();
      });
    }
    // return response; // TODO : remplacer par un return si le test fonctionne
  },
};


/*
"   {  "métiers": [    {"job": "Développeur web","jobDescription": "Le développeur web a pour mission de concevoir et développer les sites internet et applications web, intégrer les éléments graphiques et développer la partie fonctionnelle avec les langages web.","criteria": [        {    "name": "work in a big city",    "description": "La majorité des développeurs web travaillent dans des grandes villes telles que Paris, Berlin ou New York."        },        {    "name": "alcool",    "description": "Le développeur web peut travailler dans le secteur de l'alcool, par exemple en concevant des applications web pour gérer les données de production."        },        {    "name": "technology",    "description": "Le développeur web est un spécialiste de la technologie, il doit donc être au courant des dernières avancées en matière de langages web et de développement web."        }      ],"jobSalary": "environ 3500 €","jobRequirements": [        {    "description": "DUT informatique"        },        {    "description": "licence informatique"        },        {    "description": "master informatique"        }      ]    },    {"job": "Développeur application mobile","jobDescription": "Le développeur d’applications mobiles conçoit et développe des applications pour les téléphones portables et les tablettes.","criteria": [        {    "name": "work in a big city",    "description": "La majorité des développeurs d'applications mobiles travaillent dans des grandes villes telles que Paris, Berlin ou New York."        },        {    "name": "alcool",    "description": "Le développeur d'applications mobiles peut travailler dans le secteur de l'alcool, par exemple en concevant une application mobile pour suivre les données de production."        },        {    "name": "technology",    "description": "Le développeur d'applications mobiles est un spécialiste de la technologie, il doit donc être au courant des dernières avancées en matière de développement d'applications mobiles."        }      ],"jobSalary": "environ 3000 €","jobRequirements": [        {    "description": "DUT informatique"        },        {    "description": "licence informatique"        },        {    "description": "master informatique"        }      ]    }  ]}"
*/