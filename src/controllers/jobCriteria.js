const userModel=require('../models/users');
const jobCardsModel = require('../models/jobCards');
const jobCriteriaModel = require('../models/jobCriteria');
const jobRequirementsModel = require('../models/jobRequirements');
const has = require('has-keys')
const { assert } = require('../util/assert');
const { hash } = require('bcrypt');

module.exports = {

  async getJobCriteria (req, res) {
    // #swagger.criteria = ['Users']
    // #swagger.summary = 'Get All users'

    let criteria = {};
    if (has(req.params, 'criteriaId')) {
      criteria = await jobCriteriaModel.findAll({ where: { id : parseInt(req.params.criteriaId)}})
    } 
    else if (has(req.params, 'jobId')) {
      criteria = await jobCriteriaModel.findAll({ where: { jobId : parseInt(req.params.jobId)}})
    }
    // if (has(req.params, 'username')){
    //   const jobs = await jobCardsModel.findAll(
    //     { include: 
    //       { 
    //         model : jobCriteriaModel, 
    //         required: true, 
    //       }, 
    //       where : {userId : req.data.userId},
    //     }
    //   )
    //   console.log("A FAIRE")
    //   assert(false, 'a faire')
      //TODO: criteria = {...jobs.get}
    // }
    else {
      criteria = await jobCriteriaModel.findAll()
    } 
    //Envoie de la réponse
    if (criteria === null || Object.keys(criteria).length === 0) res.json({ status: 404, message: 'No criteria found' })
    else res.json({ status: true, message: 'Returning criteria', criteria })
  },
};
